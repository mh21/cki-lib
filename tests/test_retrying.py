"""Gitlab api interaction tests."""
import unittest
from unittest import mock

from cki_lib.retrying import retrying_on_exception


def method2retry(i):
    """Mock of a real function that raises exception on some condition."""
    if i == 0:
        raise AttributeError

    return 'ran'


def ignore_stderr(*args, **kwargs):
    """Mocks logging.warning, discard stderr."""
    _, _ = args, kwargs  # pylint disable=W0613 doesn't work here


class TestRetrying(unittest.TestCase):
    """Test retrying class."""

    @mock.patch('logging.warning', ignore_stderr)
    def test_retrying_different_exc(self):
        """Make sure that different types of exceptions are passed."""

        method2test = retrying_on_exception(RuntimeError)(method2retry)

        with self.assertRaises(AttributeError):
            self.assertTrue(method2test(0) is None)

    @mock.patch('logging.warning', ignore_stderr)
    def test_retrying_repeat(self):
        """Make sure repeat works."""
        retries = 3
        method2test = retrying_on_exception(AttributeError, retries=retries,
                                            initial_delay=0.001)(method2retry)

        with self.assertRaises(AttributeError):
            method2test(0)

        self.assertTrue(method2test.failed_count == retries)
