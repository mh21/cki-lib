"""Test krb_ticket_refresher script."""
import subprocess
import unittest
from unittest import mock

from freezegun import freeze_time

from cki_lib import krb_ticket_refresher


class TestRefreshKerberosTicket(unittest.TestCase):
    """Test RefreshKerberosTicket class."""

    @mock.patch('cki_lib.krb_ticket_refresher.subprocess.check_output')
    @mock.patch('cki_lib.krb_ticket_refresher.LOGGER')
    def test__close_to_expire_ticket(self, mock_logger, mock_check):
        """Test __close_to_expire_ticket."""
        mock_check.return_value = b'renew until 05/26/23 10:41:04\n05/19/23 13:05:01'
        rkt = krb_ticket_refresher.RefreshKerberosTicket()
        rkt.LOGGER = mock_logger
        # < 10h to expire
        with freeze_time('2023-05-26T08:41:04'):
            result = rkt._close_to_expire_ticket(10)
            mock_check.assert_called_once_with(["klist"])
            mock_logger.warning.assert_not_called()
            self.assertTrue(result)

        # > 10h to expire
        with freeze_time('2023-05-20T10:41:04'):
            result = rkt._close_to_expire_ticket(10)
            self.assertEqual(mock_check.call_count, 2)
            mock_logger.warning.assert_not_called()
            self.assertFalse(result)

        # Fail to parse `klist` output
        mock_check.return_value = b'foo bar'
        with freeze_time('2023-05-20T10:41:04'):
            result = rkt._close_to_expire_ticket(10)
            self.assertEqual(mock_check.call_count, 3)
            mock_logger.warning.assert_called_with("Unable to parse 'klist' output.")
            self.assertFalse(result)

    @mock.patch('cki_lib.krb_ticket_refresher.RefreshKerberosTicket'
                '._close_to_expire_ticket',
                lambda *args: False)
    @mock.patch('cki_lib.krb_ticket_refresher.subprocess.run')
    def test_run(self, mock_subprocess):
        """Test run function."""
        krb_ticket_refresher.RefreshKerberosTicket().run()
        mock_subprocess.assert_called_with(['/usr/bin/kinit', '-R'], check=True)

    @mock.patch('cki_lib.krb_ticket_refresher.retrying.time.sleep', lambda t: None)
    @mock.patch('cki_lib.krb_ticket_refresher.RefreshKerberosTicket'
                '._close_to_expire_ticket',
                lambda *args: False)
    @mock.patch('cki_lib.krb_ticket_refresher.subprocess.run')
    def test_run_failure(self, mock_subprocess):
        """Test that `kinit -R` is retried on failure."""
        call_args = ['/usr/bin/kinit', '-R']
        # The ticket refresh is attempted a maximum of 3 times.  To test this
        # behavior, force the first two calls to subprocess.run to fail, and
        # ensure that the third call succeeds.
        mock_subprocess.side_effect = (
            [subprocess.CalledProcessError(1, call_args)] * 2 +
            [subprocess.CompletedProcess(args=call_args, returncode=0)]
        )
        krb_ticket_refresher.RefreshKerberosTicket().run()
        self.assertEqual(mock_subprocess.call_count, 3)
        mock_subprocess.assert_has_calls(
            mock_subprocess.run(call_args, check=True)
        )

    @mock.patch.dict('os.environ', {'KRB_KEYTAB': '/keytab', 'KRB_USER': 'foo'})
    @mock.patch('cki_lib.krb_ticket_refresher.retrying.time.sleep',
                lambda t: None)
    @mock.patch('cki_lib.krb_ticket_refresher.'
                'RefreshKerberosTicket._close_to_expire_ticket',
                lambda *args: True)
    @mock.patch('cki_lib.krb_ticket_refresher.subprocess.run')
    def test_run_new_ticket(self, mock_subprocess):
        """Test when `run` creates a new ticket."""
        call_args = ['/usr/bin/kinit', '-t', '/keytab', 'foo', '-l', '7d']
        # The ticket creation is attempted a maximum of 3 times.  To test this
        # behavior, force the first two calls to subprocess.run to fail, and
        # ensure that the third call succeeds.
        mock_subprocess.side_effect = (
            [subprocess.CalledProcessError(1, call_args)] * 2 +
            [subprocess.CompletedProcess(args=call_args, returncode=0)]
        )
        krb_ticket_refresher.RefreshKerberosTicket().run()
        self.assertEqual(mock_subprocess.call_count, 3)
        mock_subprocess.assert_has_calls(
            mock_subprocess.run(call_args, check=True)
        )
